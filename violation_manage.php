<?php include "admin_header.php"; ?>

<h1 class="page-header">Violations</h1>

<!-- main content -->

<div class="box-content">

	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white list"></i><span class="break"></span>Student Violation Summary</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
						  <th> Student ID</th>
						  <th> Student Name</th>
						  <th> Course</th>
						  <th> Year</th>
						  <th> Section</th>						  
						  <th> Violation</th>
						  <th> Date</th>
					  </tr>
				  </thead>   
				  <tbody>
				  <?php 

			$student = distint_violation("tbl_violation","tbl_students");
			foreach ($student as $key => $row) {
			$fullname = $row['lastname'].", ".$row['firstname']." ".$row['middlename'];
			$student_id = $row['student_id'];
			$course = $row['course'];
			$year = $row['year'];
			$section = $row['section'];
			?>

					<tr>
						<td class="center"><?= $student_id ?></td>
						<td class="center"><?= $fullname ?></td>
						<td class="center"><?= $course ?></td>
						<td class="center"><?= $year ?></td>
						<td class="center"><?= $section ?></td>

			<?php
						//for violations per student
						$sql_student = violation_student($student_id);
						$rowcount=mysqli_num_rows($sql_student);

						// violation column
						echo '<td>';

						foreach ($sql_student as $key => $row) {
						$violation = $row['violation'];

						switch ($violation) {
							case 'A':
								echo "A. Haircut/punky hair ";
								break;
							case 'B':
								echo "B. Coloured Hair ";
								break;
							case 'C':
								echo "C. Unprescribed Undergarment ";
								break;
							case 'D':
								echo "D. Unprescribed Shoes ";
								break;
							case 'E':
								echo "E. Long/Short Skirt ";
								break;
							case 'F':
								echo "F. Being noisy along corridors";
								break;
							case 'G':
								echo "G. Not wearing of ID Properly";
								break;
							case 'H':
								echo "H. Earring/Tounge Ring";
								break;
							case 'I':
								echo "I. Wearing of Cap inside the Campus";
								break;
								}
								echo '<br>';
							}
						echo '</td>';

						// date column
						echo '<td >';

						foreach ($sql_student as $key => $row) {
						$date = $row['date'];
								echo $date.'<br>';
						}
						echo '</td>';
						echo '</tr>';
			
				}
					?>
				  </tbody>
				</table> 
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>