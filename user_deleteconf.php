<?php include "admin_header.php"; ?>

<h1 class="page-header">Student Comply</h1>

<!-- main content -->

<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header" data-original-title>
			<h2><i class="halflings-icon white edit"></i><span class="break"></span>Comply</h2>
		</div>


		<div class="box-content">
				<?php $form_location = base_url()."user_create_proc.php"; ?>
				<form class="form-horizontal" method="post" action="<?= $form_location ?>">
					<fieldset>

						<div class="control-group">
							<label class="control-label">Date complied:</label>
							<div class="controls">
								<input type="text" class="span4" name="datev" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Summary of Compliance:</label>
							<div class="controls">
								<textarea name="text" style="resize:none;" id="textarea2" rows="4"></textarea>
							 </div>
						</div>

						
						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="submit" value="Submit">Save</button>
							<a class="btn" href="user_manage.php">Cancel</a>
						</div>
						
						</div>


			  </fieldset>
			</form> 
		</div>
	</div><!--/span-->
</div><!--/row-->

<!-- close main content -->
<?php include "admin_footer.php"; ?>