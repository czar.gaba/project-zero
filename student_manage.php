<?php include "admin_header.php"; ?>

<h1 class="page-header">Students</h1>

<!-- main content -->

<div class="box-content">

	<?php $create_user_url = base_url()."student_create.php"; ?>
	<p>
		<a href="<?= $create_user_url ?>">
			<button type="button" class="btn btn-primary"> &nbsp; &nbsp; Add Student &nbsp; &nbsp;</button>
		</a>
	</p>


	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white list"></i><span class="break"></span>Record</h2>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered bootstrap-datatable datatable">
				  <thead>
					  <tr>
					  	  <th> SY- Sem</th>
						  <th> Student ID</th>
						  <th> Student Name</th>
						  <th> Course</th>
						  <th> Year</th>
						  <th> Status</th>
						  <th >Actions</th>
					  </tr>
				  </thead>   
				  <tbody>
				  <?php 
				  		$table_name = "tbl_students";

				  		//get all records from users table
						$user_data = get($table_name);

						//fetch result set and pass it to an array (associative)
				  		foreach ($user_data as $key => $row) {
				  		$id = $row['id'];
						$fisrtname = strtoupper($row['firstname']);
						$mname = strtoupper($row['middlename']);
						$lastname = strtoupper($row['lastname']);
						$course = strtoupper($row['course']);
						$year = $row['year'];		
						$studentid = $row['student_id'];
						$schoolyear = $row['school_year'];
						$sem = $row['sem'];
				  		$edit_user_url = base_url().'student_edit.php?id='.$id;
				  		$delete_user_url = base_url().'student_deleteconf.php?id='.$id;
				  		$show_violation_url = base_url().'show_violation.php?id='.$id;
				  ?>
					<tr>
						<td class="center"><?= $schoolyear." ".$sem ?></td>
						<td class="center"><?= $studentid ?></td>
						<td class="center"><?= $lastname.", ".$fisrtname." ".$mname ?></td>
						<td class="center"><?= $course ?></td>
						<td class="center"><?= $year ?></td>
						<td class="center">
							<a href='<?= $show_violation_url ?>' class='btn btn-success btn-sm'><i class='halflings-icon white folder-open'></i> show violation</a></td>
						<td class="center">
							<a class="btn btn-warning" href="<?= $edit_user_url ?>">
								<i class="halflings-icon white edit"></i>
							</a>
							<a class="btn btn-danger" href="<?= $delete_user_url ?>">
								<i class="halflings-icon white trash"></i> 
							</a>
						</td>
					</tr>
					<?php } ?>
				  </tbody>
				</table> 
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>