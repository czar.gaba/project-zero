<?php include "admin_header.php"; ?>

<h1 class="page-header">Add Violation</h1>

<?php
	//get user ID from URL

	$id = $_GET['id'];

	$table_name = "tbl_students";

	if ($_SERVER['REQUEST_METHOD'] === 'POST') {
	    $id = $_POST['wew'];
	}

	$getData = get_where($table_name, $id);


	foreach ($getData as $key => $row) {
		$id = $row['id'];
		$studentid = $row['student_id'];
		$firstname = $row['firstname'];
		$mname = $row['middlename'];
		$lastname = $row['lastname'];
		$fullname= $lastname.", ".$firstname." ".$mname;
		$course = $row['course'];
		$year = $row['year'];
		$cy=$course." - ".$year;
		$cancel_url = base_url().'show_violation.php?id='.$id;
	}
		
		//start of insert
				
			$success = 0;
			if(($_SERVER["REQUEST_METHOD"] == "POST") && (isset($_POST['save']) == "Send")){
				
				$viol = $_POST["viol"];

				foreach($viol as &$value)
				{
					$zz = $_POST["studid"];
					
					$sanction_date = $_POST["datev"];
					//$sanction_date = mysqli_real_escape_string($con, $_POST["sanction_date"]);

					$remarks = $_POST["remarks"];

					
				//CHECK IF STUDENT SANCTION EXIST
					
					$result= violation_count($value,$sanction_date);

					if ($result->num_rows > 0){
						echo "<div class='alert alert-danger'>Sorry, Violation ".$value." already exists.</div>";
					}
					else {
					// INSERT DATA
						$violation_data = array(
								"violation" => $value, 
								"student_id" => $zz,
								"date" => $sanction_date  
								);

						insert($violation_data, "tbl_violation");

						$whomai = _get_username_from_id($_SESSION['user_id']);
						$text = "User $whomai has successfully Added a violation for $zz";
						save_logs($text);
						$success=1;
					}
				}
				if ($success==1) {
					echo "<script> alert('Successfully added!');</script>"; 
					echo "<script>setTimeout(\"location.href = 'show_violation.php?id=".$id."';\",1);</script>";
				} 
			}
	//end of insert


	?>

<div class="box-content">

	<div class="row-fluid sortable">	
		<div class="box span12">
			<div class="box-header" data-original-title>
				<h2><i class="halflings-icon white file"></i><span class="break"></span>Add Violation(s)</h2>
			</div>

			<div class="box-content">
				<form class="form-horizontal" method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
					<fieldset>
						<input type="hidden" name="wew" value="<?php echo $id;?>">
						<input type="hidden" name="studid" value="<?php echo $studentid;?>">
						<div class="control-group">
							<label class="control-label">Student ID:</label>
							<div class="controls">
								<input type="text" class="span4" name="studentid" value="<?= $studentid ?>" readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Fullname:</label>
							<div class="controls">
								<input type="text" class="span4" style="text-transform: uppercase;" value="<?= $fullname ?>"readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Course and Year Level:</label>
							<div class="controls">
								<input type="text" class="span4" style="text-transform: uppercase;" value="<?= $cy ?>"readonly>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label">Date of violation:</label>
							<div class="controls">
								<input type="date" class="span4" name="datev" required>
							</div>
						</div>

						<div class=""control-group"">
							<label class="control-label">Select Sanction:</label>
							<div class="controls">
								<label>
									<input type="checkbox" name="viol[]" value="A"> A. Haircut/punky hair (Male) <br/>
									<input type="checkbox" name="viol[]" value="B"> B. Coloured Hair (Male/Female) <br/>
									<input type="checkbox" name="viol[]" value="C"> C. Unprescribed Undergarment (Male/Female) <br/>
									<input type="checkbox" name="viol[]" value="D"> D. Unprescribed Shoes (Male/Female) <br/>
									<input type="checkbox" name="viol[]" value="E"> E. Long/Short Skirt (Female) <br/>
									<input type="checkbox" name="viol[]" value="F"> F. Being noisy along corridors <br/>
									<input type="checkbox" name="viol[]" value="G"> G. Not wearing of ID Properly  <br/>
									<input type="checkbox" name="viol[]" value="H"> H. Earring/Tounge Ring <br/>
									<input type="checkbox" name="viol[]" value="I"> I. Wearing of Cap inside the Campus <br/>
								</label>
							</div>
						</div>

						

						<div class="form-actions">
							<button type="submit" class="btn btn-primary" name="save" value="Save">Save changes</button>
							<a class="btn" href="<?= $delete_user_url ?>">Cancel</a>
						</div>
						
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- close main content -->

<?php include "admin_footer.php"; ?>