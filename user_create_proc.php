<?php

session_start();
include "perfect_function.php";

$table_name = "users";

$id = $_POST['id'];
$firstname = $_POST['firstname'];
$mi = $_POST['mi'];
$lastname = $_POST['lastname'];
$course = $_POST['course'];
$year = $_POST['year'];
$datev = $_POST['datev'];
$violation = $_POST['violation'];
$summary = $_POST['summary'];

$user_data = array(
	//columname from table => value from post
			"id" => $id, 
			"firstname" => $firstname,
			"mi" => $mi,  
			"lastname" => $lastname, 
			"course" => $course, 
			"year" => $year, 
			"datev" => $datev, 
			"violation" => $violation,
			"summary" => $summary

);

insert($user_data, $table_name);
$recent_id = get_max($table_name);
$whomai = _get_username_from_id($_SESSION['user_id']);

$text = "User $whomai has successfully added a user";
$text.= " with an ID of $recent_id";
save_logs($text);
header("Location: user_manage.php");
?>