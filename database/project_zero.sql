-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 10, 2019 at 10:40 PM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_zero`
--

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `text` text NOT NULL,
  `datetime` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `text`, `datetime`) VALUES
(1, 'User fmasigan has successfully logged in.', 1524619523),
(2, 'User cpal has successfully logged in.', 1524619881),
(3, 'User rcalizar has successfully logged in.', 1524619891),
(4, 'User zgavino has successfully logged in.', 1524619898),
(5, 'User cbabaran has successfully logged in.', 1524619917),
(6, 'User cbabaran has successfully logged in.', 1525080446),
(7, 'User cbabaran has successfully logged in.', 1525080615),
(8, 'User rcalizar has successfully logged in.', 1525081742),
(9, 'User cbabaran has successfully logged in.', 1525082620),
(10, 'User cbabaran has successfully logged in.', 1525220728),
(11, 'User rpugeda has successfully logged in.', 1568522430),
(12, 'Admin rpugeda has successfully logged in with an ID of 32', 1568524199),
(13, 'Admin rpugeda has successfully logged in with an ID of 32', 1568524320),
(14, 'Admin rpugeda has successfully logged in with an ID of 2', 1568527776),
(15, 'Admin rpugeda has successfully logged in with an ID of 2', 1568527859),
(16, 'Admin rpugeda has successfully logged in with an ID of 2', 1568527966),
(17, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528083),
(18, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528178),
(19, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528203),
(20, 'Admin  has successfully logged in with an ID of 2', 1568528364),
(21, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528372),
(22, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528532),
(23, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528649),
(24, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528744),
(25, 'Admin rpugeda has successfully logged in with an ID of 2', 1568528782),
(26, 'User  has successfully edited a user with an ID of 2', 1568539760),
(27, 'User  has successfully edited a student with an ID of 235', 1568540259),
(28, 'User  has successfully edited a student with an ID of 235', 1568540276),
(29, 'User  has successfully edited a student with an ID of 235', 1568540403),
(30, 'User  has successfully edited a student with an ID of 235', 1568540440),
(31, 'User  has successfully edited a student with an ID of 235', 1568540715),
(32, 'User  has successfully edited a student with an ID of 235', 1568540811),
(33, 'User  has successfully edited a student with an ID of 235', 1568540979),
(34, 'User rpugeda has successfully added a user with an ID of 2', 1568542149),
(35, 'User rpugeda has successfully added a student with an ID of 237', 1568542538),
(36, 'User  has successfully edited a student with an ID of 237', 1568542667),
(37, 'User rpugeda has successfully deleted a user with an ID of 2', 1568542727),
(38, 'User rpugeda has successfully deleted a user with an ID of 2', 1568542749),
(39, 'User rpugeda has successfully deleted a user with an ID of 235', 1568542841),
(40, 'User rpugeda has successfully edited a student with an ID of ', 1568551401),
(41, 'User rpugeda has successfully edited a student with an ID of ', 1568551509),
(42, 'User rpugeda has successfully edited a student with an ID of ', 1568551840),
(43, 'User rpugeda has successfully complied a student', 1568551976),
(44, 'User rpugeda has successfully complied a student', 1568552050),
(45, 'User rpugeda has successfully complied a student', 1568552060),
(46, 'User rpugeda has successfully complied a student', 1568552062),
(47, 'User rpugeda has successfully complied a student', 1568552513),
(48, 'User rpugeda has successfully complied a student', 1568552517),
(49, 'User rpugeda has successfully complied a student', 1568552580),
(50, 'User rpugeda has successfully complied a student', 1568552583),
(51, 'User rpugeda has successfully complied a student', 1568552606),
(52, 'User rpugeda has successfully complied a student', 1568552616),
(53, 'User rpugeda has successfully complied a student', 1568552618),
(54, 'User rpugeda has successfully complied a student', 1568552621),
(55, 'User rpugeda has successfully complied a student', 1568552623),
(56, 'User rpugeda has successfully complied a student', 1568552624),
(57, 'User rpugeda has successfully complied a student', 1568552625),
(58, 'User rpugeda has successfully complied a student', 1568552626),
(59, 'User rpugeda has successfully complied a student', 1568552627),
(60, 'User rpugeda has successfully complied a student', 1568552628),
(61, 'User rpugeda has successfully complied a student', 1568552629),
(62, 'User rpugeda has successfully complied a student', 1568552630),
(63, 'User rpugeda has successfully complied a student', 1568552630),
(64, 'User rpugeda has successfully complied a student', 1568552631),
(65, 'User rpugeda has successfully complied a student', 1568552672),
(66, 'User rpugeda has successfully complied a student', 1568552674),
(67, 'User rpugeda has successfully complied a student', 1568552679),
(68, 'User rpugeda has successfully complied a student', 1568552841),
(69, 'User rpugeda has successfully added a student with an ID of 238', 1568557202),
(70, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561100),
(71, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561100),
(72, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561195),
(73, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561195),
(74, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561204),
(75, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561265),
(76, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561542),
(77, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561735),
(78, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561757),
(79, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561757),
(80, 'User rpugeda has successfully Added a violation for 2016-01648', 1568561757),
(81, 'Admin  has successfully logged in with an ID of 2', 1570737923),
(82, 'Admin  has successfully logged in with an ID of 2', 1570738007),
(83, 'Admin  has successfully logged in with an ID of 2', 1570738184),
(84, 'Admin  has successfully logged in with an ID of 2', 1570738488),
(85, 'Admin admin has successfully logged in with an ID of 2', 1570739464);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `id` int(11) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `course` varchar(50) NOT NULL,
  `year` varchar(11) NOT NULL,
  `section` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(20) NOT NULL,
  `gender` char(1) NOT NULL,
  `archive` char(1) NOT NULL DEFAULT '0',
  `school_year` varchar(50) NOT NULL,
  `sem` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`id`, `student_id`, `firstname`, `middlename`, `lastname`, `course`, `year`, `section`, `email`, `contact`, `gender`, `archive`, `school_year`, `sem`) VALUES
(1, '2018-01-0317', 'Johanes Paulus', 'D', 'Abuyuan', 'BSIT', '6', 'B', 'dfgdfg', 'dfgdfg', 'M', '0', '2019-2020', '2nd'),
(2, '2016-01648', 'Joyan Noel', 'M', 'Aggarao', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(3, '2018-01-0001', 'Paul Arwin', 'Suguitan', 'Alda', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(4, '2013-00221', 'Gerald Martin', 'V', 'Balabis', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(5, '2016-00635', 'Bryan', 'Q', 'Baliuag', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(6, '2018-01-1222', 'Dan Angelo', 'Q', 'Baliuag', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(7, '2016-01646', 'Kurt Roland', 'M', 'Bayadog', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(8, '2018-01-0678', 'Orlando Jr.', 'C', 'Bunagan', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(9, '2012-00010', 'Vince Lesther', 'A', 'Calimag', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(10, '2018-01-1000', 'Darren Angelo', 'S', 'Callueng', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(11, '2018-01-0857', 'Ceazar Christ', 'A', 'Canceran', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(12, '2018-01-1016', 'Arman Paul Jr.', 'A', 'Cortes', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(13, '2016-01745', 'Kelvin Kharl', 'S', 'Dabo', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(14, '2016-01487', 'Keith Angelo', 'D', 'De Leon', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(15, '2018-01-1108', 'Florido III', 'A', 'Deray', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(16, '2016-01099', 'Kim Randel', 'D', 'Dolozon', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(17, '2018-01-0030', 'David ', 'A', 'Domaguina', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(18, '2018-01-0074', 'Joshua', 'C', 'Dumbrique', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(19, '2012-00001', 'Miguel Francisco', '', 'Gamiao', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(20, '2012-02299', 'Elymar', '', 'Gongob', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(21, '2016-00771', 'Granduer Majesty', 'T', 'Labang', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(22, '2018-01-1265', 'Joh Karl', 'F', 'Labang', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(23, '2018-01-1040', 'Jerick Steven', 'J', 'Lara', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(24, '2018-01-1018', 'Allen Paul', 'F', 'Liong', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(25, '2018-01-0044', 'Michael Angelo', 'B', 'Macarubbo', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(26, '2018-01-0975', 'Fritz Allen Rae', 'L', 'Malana', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(27, '2018-01-1150', 'Zeus Marou', 'B', 'Marigocio', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(28, '2018-01-0006', 'Renz Christian', 'C', 'Mesa', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(29, '2016-01050', 'Rodston', 'R', 'Millare', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(30, '2004-00037', 'Francis Prim', 'S', 'Pagunuran', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(31, '2018-01-1095', 'Lito John', 'F', 'Portos', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(32, '2003-00194', 'Jonas Gabriel', 'C', 'Rivera', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(33, '2006-10556', 'Alec Jiro', 'C', 'Saludes', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(34, '2017-030043', 'Jean Lloyd', 'B', 'Taguba', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(35, '2018-01-0037', 'Jowel', 'C', 'Utanes', 'BSIT', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(36, '2016-01080', 'Wardita Marie', 'R', 'Agustin', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(37, '2016-00681', 'Cassandra Marie', 'C', 'Andal', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(38, '2016-01735', 'Jay Vee', 'S', 'Apostol', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(39, '2016-01677', 'Maricar', 'B', 'Arzadon', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(40, '2018-01-0023', 'Mary Joyce', 'H', 'Baletan', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(41, '2018-01-1203', 'Ritchelle Joy', 'D', 'Catabian', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(42, '2016-00701', 'Thaniela Cayl', 'S', 'Chan', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(43, '2016-00612', 'Marcy Clarete', '', 'Clemente', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(44, '2018-01-0052', 'Jessa', 'R', 'Parungao', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(45, '2018-01-0010', 'Mary Xylex', 'B', 'Pasion', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(46, '2018-01-1232', 'Julie Ann', 'A', 'Ramil', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(47, '2018-01-0756', 'Athena Mae', 'B', 'Santiago', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(48, '2018-01-1221', 'Naomi', 'S', 'Tanaka', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(49, '2018-01-0914', 'Jezarene', 'C', 'Valiente', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(50, '2016-00785', 'Alyssa Jane', 'Masirag', 'Villanueva', 'BSIT', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(51, '2015-01060', 'Adrinel Shane', 'T', 'Abella', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(52, '2015-00135', 'Kurt Kovien', 'B', 'Acoba', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(53, '2014-00629', 'Christian Paul', 'T', 'Agpuon', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(54, '2014-00756', 'Daniel', '', 'Bangayan', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(55, '2014-00436', 'Tomas Jr.', 'M', 'Binag', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(56, '2015-00572', 'Wendelle', 'A', 'Buncag', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(57, '2012-02-02337', 'Markavin  ', '', 'Cabildo', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(58, '2015-00526', 'Vincent Carl', 'S', 'Caddauan', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(59, '2015-00675', 'Rheiner', 'C', 'Calizar', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(60, '2012-01-01735', 'Christian Jay', '', 'Carag', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(61, '2015-00520', 'Isabelo Jr.', 'A', 'Cari?o', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(62, '2014-00933', 'Arnel', 'D', 'Casay', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(63, '2017-0100270', 'Adrian Louis', 'E', 'Casibang', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(64, '2015-01013', 'Karl Patrick', 'C', 'Cellona', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(65, '2015-00127', 'Joel John', 'De Guzman', 'Centeno', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(66, '2015-01073', 'Russel Kim Alexis', 'Despabelade', 'Cristobal', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(67, '2017-030015', 'Christian Marie', 'L', 'Danao', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(68, '2017-0100276', 'Leorex Keith', 'W', 'Danao', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(69, '2015-00638', 'Seth Joshua', 'M', 'Dannang', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(70, '2015-00603', 'Vincent Cenen', 'Dionisio', 'Dayag', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(71, '2015-00956', 'John Paul', 'Patricio', 'Dulin', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(72, '2016-2-00089', 'Diego Gracias', 'Acera', 'Dumaua', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(73, '2015-00607', 'King Shalomrabi', 'Mabborang', 'Feliciano', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(74, '2015-00699', 'Aris June', 'Pamittan', 'Fernandez', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(75, '2015-00706', 'Mark Darylle', 'Apostol', 'Formose', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(76, '2015-00347', 'Brian', 'Agustin', 'Foronda', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(77, '2015-01239', 'Jaylord', 'Turingan', 'Frace', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(78, '2016-00120', 'Christian', 'Maggay', 'Francinilla', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(79, '2011-02-01230', 'Raymond', 'Zu?iga', 'Gamiao', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(80, '2015-00051', 'Zahn Xandre', 'Acorda', 'Gavino', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(81, '2016-2-00085', 'Joemel', 'Umblas', 'Gersaniba', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(82, '2014-00392', ' Joshua James', 'Talattad', 'Ginez', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(83, '2015-01020', 'Brylle Geno', 'Sanchez', 'Gumabay', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(84, '2015-00785', 'Eljone', 'Yoma', 'Hermano', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(85, '2017-0100277', 'Juan Miguel Rafael', 'P', 'Iba?ez', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(86, '2015-00713', 'Prince Gio', 'Pagaduan', 'Lacsamana', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(87, '2017-03-0018', 'Mark Bryan', 'Calayan', 'Lagundi', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(88, '2016-2-00084', 'Joseph Carlo', 'M', 'Liban', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(89, '2015-00843', 'John Kenneth', 'Cannu', 'Mabazza', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(90, '2015-00269', 'Daniel', 'Agustin', 'Mabborang', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(91, '2015-00710', 'Reuel Jascha', 'Guibani', 'Mabborang', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(92, '2015-00608', 'Kevin Enryll', 'Miguel', 'Maggay', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(93, '2012-02-02285', 'John Rovan', 'Abedes', 'Mallillin', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(94, '2015-00024', 'Mario Emmanuel', 'Morales', 'Mallillin', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(95, '2016-2-00092', 'Jasper', 'Malenab', 'Melad', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(96, '2017-030015', 'Ervin Roi', 'C', 'Mesina', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(97, '2017-0100312', 'Jefferson', 'P', 'Oledan', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(98, '2015-00962', 'Mark Anthony', 'Pimentel', 'Orence', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(99, '2015-00674', 'John Bernard', 'Pastera', 'Orpilla', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(100, '2015-00200', 'Jules Lemuel', 'Tabaldo', 'Pastoral', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(101, '2015-00414', 'Dlanorson', 'Teja', 'Patay', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(102, '2015-00678', 'Keith Russel', 'Delmendo', 'Que', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(103, '2016-00035', 'Jan Giled', 'Barnedo', 'Reyes', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(104, '2014-00757', 'Evanz', 'Espita', 'Saguibo', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(105, '2013-01-03360', 'Constante Jr.', 'Recolizado', 'Sapla', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(106, '2014-00554', 'John Paul ', 'Melad', 'Silvestre', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(107, '2015-00107', 'Jayvee', 'Cabaddu', 'Siuagan', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(108, '2014-00857', 'Homer Charles', 'Acebedo', 'Tillenuis', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(109, '2013-01-03123', 'John Rico ', 'Corral', 'Toribio', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(110, '2015-00948', 'Mark Angelo', 'Mactal', 'Torres', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(111, '2015-00120', 'Ralph Gabriel', 'Ballad', 'Tuapin', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(112, '2015-00625', 'Asher Winfrey', 'Agustin', 'Udarbe', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(113, '2015-00321', 'Ralph Lauren-j', 'Arcines', 'Viernes', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(114, '2015-00248', 'Allan Jay', 'Masirag', 'Villanueva', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(115, '2015-00541', 'Marc Angelo', 'Sablay', 'Yadao', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(116, '2016-00031', 'Marulan', '', 'Yuga', 'BSIT', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(117, '2016-00048', 'Criselda', 'Sumaoang', 'Abad', 'BSIT', '4', '', '', '', 'F', '0', '2019-2020', '1ST'),
(118, '2015-00874', 'Ann Marie Kristine', 'Guerrero', 'Addatu', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(119, '2015-00324', 'Reina', 'Soller', 'Altura', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(120, '2015-00057', 'Kristel Joy', 'Lucas', 'Apelado', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(121, '2016-00039', 'Jeinin Lei', 'Lustica', 'Arellano', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(122, '2015-00292', 'Odhessa Ross', 'Batan', 'Barba', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(123, '2015-01279', 'Shaina Mae', 'Sabado', 'Beligan', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(124, '2015-00307', 'Junross Gaile', 'Dela Cruz', 'Binarao', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(125, '2017-0100273', 'Ma. Angelica', 'G', 'Callangan', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(126, '2015-01085', 'Regina Joyce', 'Llobrera', 'Canlas', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(127, '2015-00297', 'Kharen Dayle', 'Lim', 'Cuevas', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(128, '2015-00029', 'Heidie', 'Aggabao', 'Dela Cruz', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(129, '2015-00336', 'Rolayne', 'Montelibano', 'Dela Paz', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(130, '2015-00131', 'Reni Shane', 'Pancho', 'Denna', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(131, '2015-00458', 'Jezrille Kylie', 'Bisquera', 'Espartero', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(132, '2015-00210', 'Diana', 'Magaddatu', 'Galang', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(133, '2015-00344', 'Ellaine ', 'Yoma', 'Hermano', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(134, '2015-00305', 'Jyzel', 'Conciso', 'Hernandez', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(135, '2016-2-00090', 'Noami', 'Macababbad', 'Marciano', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(136, '2016-00062', 'Sandy Anne', 'Battad', 'Mecate', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(137, '2015-00279', 'Chrysiele', 'Lizardo', 'Pal', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(138, '2017-0100295', 'Jhanika Mhae', 'Ona', 'Pe?a', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(139, '03-02676', 'Marie Monique', 'Lingan', 'Sabban', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(140, '2015-00719', 'Johana Mae', 'Arizo', 'Tangan', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(141, '2015-00108', 'Clarence Joy', 'Rojero', 'Torrado', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(142, '2016-000075', 'Kate Charmaine ', 'Bangayan', 'Viraguas', 'BSIT', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(143, '2015-00017', 'Mariel', 'Capalungan', 'Addatu', 'BLIS', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(144, '2015-00204', 'Ma. Trinidad', 'Suyu', 'Baccay', 'BLIS', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(145, '2015-00753', 'Jiberly', 'Basilio', 'Corpuz', 'BLIS', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(146, '2016-00114', 'Jale', 'Saquing', 'Valdez', 'BLIS', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(147, '2016-00055', 'Ena Alodia', 'Amistad', 'Yanga', 'BLIS', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(148, '2018-01-0111', 'Christian Darell', 'D', 'Bandayrel', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(149, '2016-00687', 'John Carlo', 'G', 'Binarao', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(150, '2018-01-1044', 'Henry Jr.', 'O', 'Caligan', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(151, '2018-01-1268', 'Ryan Eleazar', 'G', 'Calngao', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(152, '2018-01-0744', 'Paul Jacob', 'D', 'Capitle', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(153, '2018-01-0745', 'Ronald', 'C', 'Llanera', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(154, '2016-00901', 'Mark Jester', 'A', 'Nicolas', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(155, '2015-01592', 'Adrian Emil', 'Sinchioco', 'Taganas', 'BSCOE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(156, '2016-01056', 'Sarah Lei', 'C', 'Piagan', 'BSCOE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(157, '2018-01-1107', 'Micahella', 'L', 'Tamondon', 'BSCOE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(158, '2015-00569', 'Timothy John', 'Genobili', 'Alindayu', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(159, '2018-01-1238', 'Philipe Anthony', 'C', 'Battung', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(160, '2017-030028', 'John Elrom', 'V', 'Baylon', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(161, '2013-01-03104', 'John Dale', 'Guingab', 'Bulusan', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(162, '2018-01-1239', 'Romel', 'G', 'Bunagan', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(163, '2017-030013', 'Eugene Anthony', 'T', 'Columna', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(164, '2017-0100274', 'James', 'C', 'Dulin', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(165, '2013-01-02639', 'James Jr.', 'Consuelo', 'Gonzales', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(166, '2015-00862', 'Anthony', 'Datul', 'Hernandez', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(167, '2018-01-1235', 'Roger Jr.', 'T', 'Leal', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(168, '2012-01-02090', 'Mark Anthony ', 'Buraga', 'Macarubbo', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(169, '2017-030016', 'John Alex', 'M', 'Miranda', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(170, '2018-01-1237', 'Caimon Keith', 'C', 'Nool', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(171, '2017-0100293', 'Karl Aldrin', 'Bisquera', 'Paredes', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(172, '2017-030011', 'Kaymar', 'C', 'Ramachandran', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(173, '2015-00009', 'Joseph Anthony', 'Malana', 'Tabbu', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(174, '2015-01251', 'Evan', '', 'Talosig', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(175, '2013-01-03271', 'Mark Ashley', 'Cagurangan', 'Tumanguil', 'BSCOE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(176, '2016-00033', 'Pauli Angeli', 'Tulauan', 'Aguilar', 'BSCOE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(177, '2017-03-0001', 'Fatima Yvonne', 'L', 'Allam', 'BSCOE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(178, '2017-0100279', 'Maria Clarissa', 'L', 'Aquino', 'BSCOE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(179, '2015-00380', 'Kimberly', 'Sumer', 'Quejado', 'BSCOE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(180, '2015-00813', 'Patricia', 'Ejiroghene', 'Temile', 'BSCOE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(181, '2016-01165', 'Christine', 'P', 'Bancud', 'BSENSE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(182, '2016-01877', 'Jenalyn', 'Acera', 'Corpuz', 'BSENSE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(183, '2016-01111', 'Jenmizeth Kaye', 'C', 'Ludovice', 'BSENSE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(184, '2015-00501', 'Bryan', 'Hosmillo', 'Angoluan', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(185, '2015-00360', 'Aaron Henrich', 'Barrias', 'Bambalan', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(186, '2015-01257', 'Enrico', 'Ortega', 'Bernardino', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(187, '2015-00205', 'Winfreynel', 'Elizaga', 'Dadia', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(188, '2015-00665', 'Patrick Leo', 'Ruiz', 'Dela Cruz', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(189, '2015-00475', 'Jake', 'Pascua', 'Encarnacion', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(190, '2015-01277', 'Marty Niko', 'Manaligod', 'Liban', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(191, '2016-000002', 'Edward John', 'Pacis', 'Marallag', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(192, '2015-00590', 'Joshua', 'Calamasa', 'Navarro', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(193, '2013-01-02694', 'Kent Jude', 'Uman', 'Pascual', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(194, '2015-00504', 'Juben', '', 'Salvador', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(195, '2015-01256', 'Marc Dylan', '', 'Vargas', 'BSENSE', '9', '', '', '', 'M', '0', '2019-2020', '1ST'),
(196, '2013-02-03570', 'Jayanne', 'Favila', 'Apostol', 'BSENSE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(197, '2017-03-0009', 'Coleen Joy ', 'Ganipan', 'Cammayo', 'BSENSE', '9', '', '', '', 'F', '0', '2019-2020', '1ST'),
(198, '2018-01-1210', 'Angelica', 'Calipdan', 'Baquiran', 'BSGE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(199, '2018-01-0640', 'Steven Kent', 'A', 'Alonzo', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(200, '2018-01-0084', 'Anford', 'A', 'Aquino', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(201, '2012-00232', 'Carl Steven', 'R', 'Aragon', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(202, '2012-00230', 'William Harvey', 'Unida', 'Ariniego', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(203, '2018-01-0971', 'Jaybie', 'P', 'Barit', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(204, '2018-01-1165', 'Charlie', 'B', 'Bugnay', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(205, '2018-01-0709', 'Clifford', 'M', 'Clemencia', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(206, '2018-01-1169', 'Jared Kyle', 'D', 'Daguitan', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(207, '2018-01-1138', 'Kenny Mark', 'B', 'Dongan', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(208, '2018-01-0858', 'Irvin Kent', 'G', 'Echalar', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(209, '2018-01-0894', 'Paul Christian', 'R', 'Fabro', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(210, '2018-01-0742', 'Danielle Angelo', 'A', 'Fieror', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(211, '2018-01-0990', 'Charles Harold', 'R', 'Galiza', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(212, '2018-01-0076', 'Paul Clifford', 'L', 'Garcia', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(213, '2018-01-1013', 'Makk Mogim', 'J', 'Imperio', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(214, '2016-01289', 'Gio', 'U', 'Malapira', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(215, '2018-01-1039', 'Ariel Jude', 'M', 'Mangrubang', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(216, '2018-01-0086', 'Winchel', 'C', 'Mendoza', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(217, '2016-01914', 'Eliezer', 'Dela Pena', 'Orge', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(218, '2018-01-0905', 'Peter Angelo', 'R', 'Padilla', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(219, '2016-01443', 'Mark Howard', 'E', 'Parallag', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(220, '2018-01-0079', 'Keith Dylan', 'G', 'Pasion', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(221, '2018-01-0305', 'Joefferson', 'D', 'Salcedo', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(222, '2018-01-1067', 'Jason Lloyd', 'P', 'Salmo', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(223, '2018-01-1258', 'Jastine Vomett Harold', 'W', 'Tabanganay', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(224, '2018-01-1020', 'Vincent', 'V', 'Viernes', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(225, '2018-01-1228', 'Raymart', 'E', 'Villanueva', 'BSCE', '6', '', '', '', 'M', '0', '2019-2020', '1ST'),
(226, '2018-01-0927', 'Anna Cristina', 'D', 'Alvarez', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(227, '2016-01733', 'Lyka', 'Agregado', 'Aron', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(228, '2016-00994', 'Jeanne David', 'F', 'Ba?ares', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(229, '2018-01-0075', 'Relyn Mae', 'D', 'Bayani', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(230, '2016--1888', 'Leah Sheilamae', 'B', 'Cusipag', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(231, '2018-01-0087', 'Cherry Faye', 'R', 'Lamusao', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(232, '2014-00680', 'Aravel', 'Reyes', 'Lutoc', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(233, '2012-00014', 'Lorraine', 'M', 'Talattad', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(234, '2018-01-1010', 'Karren Mae', 'G', 'Umbay', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST'),
(235, '2018-01-0945', 'Pamela Jezreel', 'T', 'Villanueva', 'BSCE', '6', '', '', '', 'F', '0', '2019-2020', '1ST');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_violation`
--

CREATE TABLE `tbl_violation` (
  `id` int(11) NOT NULL,
  `violation` varchar(100) NOT NULL,
  `student_id` varchar(20) NOT NULL,
  `date` date NOT NULL,
  `b1` int(1) NOT NULL DEFAULT 0,
  `b2` int(1) NOT NULL DEFAULT 0,
  `timestamp` datetime NOT NULL DEFAULT current_timestamp(),
  `salt` varchar(255) NOT NULL,
  `remarks` varchar(65) NOT NULL,
  `student_compliance` text NOT NULL,
  `action_taken` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_violation`
--

INSERT INTO `tbl_violation` (`id`, `violation`, `student_id`, `date`, `b1`, `b2`, `timestamp`, `salt`, `remarks`, `student_compliance`, `action_taken`) VALUES
(25, 'A', '2016-00048', '2019-05-01', 1, 1, '2019-05-24 18:56:58', 'fc0b3c927d76cd3206efa00ad086f5d08c7c86bc', '1', '', ''),
(26, 'E', '2016-00048', '2019-05-01', 1, 1, '2019-05-24 18:56:58', 'fc0b3c927d76cd3206efa00ad086f5d08c7c86bc', '1', '', ''),
(27, 'I', '2016-00048', '2019-05-01', 1, 1, '2019-05-24 18:56:58', 'fc0b3c927d76cd3206efa00ad086f5d08c7c86bc', '1', '', ''),
(28, 'B', '2018-01-0317', '2019-05-02', 0, 0, '2019-05-24 18:58:45', '5c41dc6c4430c5796c663e9c2f0c3a2cab1d0ca2', '1', '', ''),
(29, 'C', '2015-01060', '2019-05-09', 1, 0, '2019-05-24 19:04:13', '5e4371dd187eb3f3bfbd0746e153f8f386e57f92', '1', '', ''),
(30, 'D', '2015-01060', '2019-05-09', 0, 1, '2019-05-24 19:04:13', '5e4371dd187eb3f3bfbd0746e153f8f386e57f92', '1', '', ''),
(31, 'F', '2015-01060', '2019-05-09', 0, 0, '2019-05-24 19:04:14', '4fdb6cd7e818ac4cb979a250a1c34a8d2c18a749', '1', '', ''),
(32, 'G', '2015-01060', '2019-05-09', 0, 0, '2019-05-24 19:04:14', '4fdb6cd7e818ac4cb979a250a1c34a8d2c18a749', '1', '', ''),
(33, 'H', '2015-01060', '2019-05-09', 0, 0, '2019-05-24 19:04:14', '4fdb6cd7e818ac4cb979a250a1c34a8d2c18a749', '1', '', ''),
(34, 'I', '2015-01060', '2019-05-09', 0, 0, '2019-05-24 19:04:14', '4fdb6cd7e818ac4cb979a250a1c34a8d2c18a749', '1', '', ''),
(35, 'F', '2015-00017', '2019-05-04', 0, 0, '2019-05-24 19:06:01', '085515b8c94efb8980432bd182ae1eabdd363dd0', '1', '', ''),
(36, 'B', '2016-00048', '2019-05-18', 1, 1, '2019-05-24 19:07:10', '145cdb7ed943a51faa04d980e31591553969233f', '1', '', ''),
(37, 'I', '2016-00048', '2019-05-25', 1, 1, '2019-05-25 13:53:52', '3e8e7fe797e1bffdca3ef8a739e62e98e00a9aa5', '2', '', ''),
(38, 'A', '2015-00135', '2019-05-28', 0, 0, '2019-05-28 00:00:37', 'b40c4acaaabc251b98b25aaa8789f52b008d557f', '1', '', ''),
(45, 'A', '2016-01648', '2019-09-15', 0, 0, '2019-09-15 23:32:22', '', '', '', ''),
(46, 'B', '2016-01648', '2019-09-15', 0, 0, '2019-09-15 23:35:35', '', '', '', ''),
(47, 'C', '2016-01648', '2019-09-15', 0, 0, '2019-09-15 23:35:57', '', '', '', ''),
(48, 'D', '2016-01648', '2019-09-15', 0, 0, '2019-09-15 23:35:57', '', '', '', ''),
(49, 'E', '2016-01648', '2019-09-15', 0, 0, '2019-09-15 23:35:57', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(65) NOT NULL,
  `email` varchar(65) NOT NULL,
  `password` varchar(125) NOT NULL,
  `firstname` varchar(65) NOT NULL,
  `lastname` varchar(65) NOT NULL,
  `acct_type` int(2) NOT NULL,
  `photo` varchar(65) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `firstname`, `lastname`, `acct_type`, `photo`, `status`) VALUES
(2, 'rpugeda', '', '$2y$11$GE/2Jg.OsWLfuoZgq1SF2e97HADOex6uENbarVIjW/eSa/7kEBAa2', 'Rucelj', 'Pugeda', 1, 'oasis.jpg', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_violation`
--
ALTER TABLE `tbl_violation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT for table `tbl_violation`
--
ALTER TABLE `tbl_violation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tokens`
--
ALTER TABLE `tokens`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
